# Python Implementation

Whilst this project does what it says on the tin, you've noticed a way in which
it can be improved. It'd be better if the programme allowed you to enter more
words before exiting, and it'd be great if it kept track of the total score.

# Running the Tests
In order to test our [scrabble scorer](scrabble_score.py) works as expected,
tests have been written that use the [pytest framework](https://docs.pytest.org/en/latest/).

To run the tests, run the appropriate command below:

    pytest scrabble_score_test.py

If this does not work, you probably don't have pytest installed as it's own package.
So, alternatively, you can try to Python to run the pytest module:

    python3 -m pytest scrabble_score_test.py

If neither of these commands work, you'll have to install Pytest on your system.
If you're using debian, try:

    sudo apt-get install python3-pytest

To verify this has installed, try `pytest --help`, and you should see the Pytest
help message.

Consult your supervisor if you need help or once you have all the tests passing.

## Common `pytest` options

- `-v` : enable verbose output
- `-x` : stop running tests on first failure
- `--ff` : run failures from previous test before running other test cases

For other options, see `python -m pytest -h`