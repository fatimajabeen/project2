#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "tiles.h"
#include "utils.h"

#define TILE_SET_COUNT 1

struct tile_bag_s
{
	tile_t tile[TILES_NUM_MAX];
	size_t index;
	bool   allocated;
};

static tile_bag_t tile_bag[TILE_SET_COUNT] = {0};

#define TILE_GET_INDEX(t) (t == '_' ? 0 : (t - 'A') + 1)
#define INDEX_GET_TILE(i) (i == 0 ? '_' : (i + 'A') - 1)
#define TILE_UNIQUE_COUNT 27

static const unsigned tile__value[TILE_UNIQUE_COUNT] =
{
	[TILE_GET_INDEX('_')] = 0,

	[TILE_GET_INDEX('A')] = 1,
	[TILE_GET_INDEX('E')] = 1,
	[TILE_GET_INDEX('I')] = 1,
	[TILE_GET_INDEX('O')] = 1,
	[TILE_GET_INDEX('U')] = 1,
	[TILE_GET_INDEX('L')] = 1,
	[TILE_GET_INDEX('N')] = 1,
	[TILE_GET_INDEX('R')] = 1,
	[TILE_GET_INDEX('S')] = 1,
	[TILE_GET_INDEX('T')] = 1,

	[TILE_GET_INDEX('D')] = 2,
	[TILE_GET_INDEX('G')] = 2,

	[TILE_GET_INDEX('B')] = 3,
	[TILE_GET_INDEX('C')] = 3,
	[TILE_GET_INDEX('M')] = 3,
	[TILE_GET_INDEX('P')] = 3,

	[TILE_GET_INDEX('F')] = 4,
	[TILE_GET_INDEX('H')] = 4,
	[TILE_GET_INDEX('V')] = 4,
	[TILE_GET_INDEX('W')] = 4,
	[TILE_GET_INDEX('Y')] = 4,

	[TILE_GET_INDEX('K')] = 5,

	[TILE_GET_INDEX('J')] = 8,
	[TILE_GET_INDEX('X')] = 8,

	[TILE_GET_INDEX('Q')] = 10,
	[TILE_GET_INDEX('Z')] = 10
};

static const char tile__base_bag[TILES_NUM_MAX + 1] =
	"__AAAAAAAAAEEEEEEEEEEEEIIIIIIIIIOOOOOOOOUUUULLLLNN"
	"NNNNRRRRRRSSSSTTTTTTDDDDGGGBBCCMMPPFFHHVVWWYYKJXQZ";


void tile_bag_shuffle(tile_bag_t* bag)
{
	if (!bag) return;

	size_t i;
	for (i = 0; i < TILES_NUM_MAX - 1; i++)
	{
		size_t j     = i + rand() / (RAND_MAX / (TILES_NUM_MAX - i) + 1);
		tile_t t     = bag->tile[j];
		bag->tile[j] = bag->tile[i];
		bag->tile[i] = t;
	}
}

static void tile_bag__reset(tile_bag_t* bag)
{
	if (!bag) return;

	static bool initialised = false;
	if (!initialised)
	{
		initialised = true;
		srand(time(NULL));
	}

	for (unsigned i = 0; i < TILES_NUM_MAX; i++)
	{
		bag->tile[i].face     = tile__base_bag[i];
		bag->tile[i].is_blank = (tile__base_bag[i] == '_');
	}

	tile_bag_shuffle(bag);
}


tile_bag_t* tile_bag_create(void)
{
	tile_bag_t* bag = NULL;

	for (unsigned i = 0; i < TILE_SET_COUNT; i++)
	{
		if (!tile_bag[i].allocated)
		{
			bag = &tile_bag[i];
			tile_bag[i].allocated = true;
			break;
		}
	}
	if (!bag)
		return NULL;

	/* Initialise bag with random order of tiles */
	tile_bag__reset(bag);

	return bag;
}


void tile_bag_delete(tile_bag_t* bag)
{
	if (!bag) return;
	bag->allocated = false;
}


tile_t tile_bag_pick(tile_bag_t* bag)
{
	if (!bag || (bag->index >= TILES_NUM_MAX))
		return (tile_t) { .face = '\0' };

	return bag->tile[bag->index++];
}

bool tile_bag_empty(const tile_bag_t* bag)
{
	if (!bag) return true;

	return (bag->index >= TILES_NUM_MAX);
}


size_t tile_bag_add(tile_bag_t* bag, const tile_t* tiles, size_t tile_count)
{
	if (!bag || !tiles || (tile_count == 0) || (tile_count > bag->index))
		return false;

	size_t added = 0;
	for (unsigned i = 0; i < tile_count; i++)
	{
		if (tiles[i].face != '\0')
		{
			bag->tile[--bag->index] = tiles[i];
			added++;
		}
	}

	return added;
}


unsigned tile_value(tile_t tile)
{
	if (!as_tile(tile.face).face) return 0;
	if (tile.is_blank)
		tile.face = '_';
	return tile__value[TILE_GET_INDEX(tile.face)];
}


tile_t as_tile(char ch)
{
	tile_t tile = {0};
	if (ch == '_')
	{
		tile.face     = ch;
		tile.is_blank = true;
	}
	else
	{
		tile.face     = letter_upper(ch);
		tile.is_blank = false;
	}

	return tile;
}


tile_rack_t tile_rack_create(void)
{
	tile_rack_t rack =
	{
		.tile = {{0}},
		.size = TILE_RACK_SIZE
	};

	return rack;
}

void tile_rack_topup(tile_rack_t* rack, tile_bag_t* bag)
{
	if (!bag || !rack) return;

	tile_t tile;
	for (unsigned i = 0; i < rack->size; i++)
	{
		if (rack->tile[i].face == '\0')
		{
			tile = tile_bag_pick(bag);
			if (tile.face == '\0')
				break;
			else
				rack->tile[i] = tile;
		}
	}
}


bool tile_rack_remove(tile_rack_t* rack, char tile_face, tile_t* out)
{
	if (!rack || !out || ((tile_face = letter_upper(tile_face)) == 0))
		return false;

	bool found = false;
	int  blank_index = -1;

	for (unsigned i = 0; i < rack->size; i++)
	{
		if (tile_face == rack->tile[i].face)
		{
			found = true;
			*out  = rack->tile[i];
			rack->tile[i] = as_tile(0);
			break;
		}
		if (rack->tile[i].is_blank && (blank_index == -1))
		{
			blank_index = i;
		}
	}

	if (!found && ((blank_index >= 0) && ((unsigned)blank_index < rack->size)))
	{
		/* Return blank with requested face value */
		*out = rack->tile[blank_index];
		out->face = tile_face;
		rack->tile[blank_index] = as_tile(0);
		found = true;
	}

	return found;
}


bool tile_rack_add(tile_rack_t* rack, tile_t tile)
{
	if (!rack) return false;

	bool added = false;

	for (unsigned i = 0; i < rack->size; i++)
	{
		if (rack->tile[i].face == '\0')
		{
			added = true;
			rack->tile[i] = tile;
			if (rack->tile[i].is_blank)
				rack->tile[i].face = '_';
			break;
		}
	}

	return added;
}


void tile_rack_clear(tile_rack_t* rack)
{
	if (!rack) return;

	for (unsigned i = 0; i < rack->size; i++)
		rack->tile[i] = as_tile(0);
}
