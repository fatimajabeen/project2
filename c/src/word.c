#include <stdio.h>

#include "word.h"
#include "tiles.h"
#include "utils.h"

#define FILE_LINE_MAX_LEN 256


size_t word_string_match(const board_word_t* word, const char* str)
{
	if (!str) return false;

	unsigned i;
	for (i = 0; (word->square[i].tile.face != '\0') && (str[i] != '\0'); i++)
	{
		if (letter_upper(word->square[i].tile.face) != letter_upper(str[i]))
			break;
	}
	return i;
}


board_word_t as_word(const char* str, char* type)
{
	board_word_t word = {0};
	bool has_type = (type && (type[0] != '\0'));
	if (!str) return word;

	unsigned i;
	for (i = 0; (str[i] != '\0') && (i < WORD_MAX_LENGTH); i++)
	{
		word.square[i].tile = as_tile(str[i]);
		word.square[i].type = has_type ? type[i] : '\0';
	}
	word.length = i;

	return word;
}


unsigned word_score(board_word_t word)
{
	(void)word;
	return 0;
	// fill me in
}


bool is_word(board_word_t word, const char* word_list_file)
{
	if (!word_list_file) return false;
	(void)word;

	return false;
	// fill me in
}
