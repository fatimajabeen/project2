#include <ncurses.h>
// override some ncurses default behaviour
#define KEY_ESC_   27
#define KEY_ENTER_ 10
#include <string.h>

#include "utils.h"
#include "ui.h"

#define UI_TILE_WIDTH    3
#define UI_TILE_TEXT_MAX (UI_TILE_WIDTH + 1)
#define UI_TILE_HEIGHT   1
#define UI_BORDER_WIDTH  2

typedef enum
{
	UI_COLOR_PAIR_NONE = 1,
	UI_COLOR_PAIR_NORM,
	UI_COLOR_PAIR_2,
	UI_COLOR_PAIR_3,
	UI_COLOR_PAIR_D,
	UI_COLOR_PAIR_T,
	UI_COLOR_PAIR_TILE,
	UI_COLOR_PAIR_TEXTBOX,
	UI_COLOR_PAIR_ERROR = UI_COLOR_PAIR_T,
	UI_COLOR_PAIR_COUNT
} ui_color_pair_e;

static bool       wipe_cursor_last = false;
static position_t cursor_last      = {0};
static position_t cursor           = {0};

static int n_row_term, n_col_term;
static unsigned board_width;
static tile_rack_t* tile_rack;

static ui_mode_e ui_mode = UI_MODE_ON_BOARD;

#define UI_TEXT_MAX 256
typedef enum
{
	UI_TEXT_BOX_CMD,
	UI_TEXT_BOX_ERROR,
	UI_TEXT_BOX_SCORE,

	UI_TEXT_BOX_COUNT
} ui_text_box_e;

typedef struct
{
	ui_color_pair_e color;
	position_t      start;
	position_t      end;
	char            text[UI_TEXT_MAX];
} ui_text_box_t;

static ui_text_box_t ui_text_box[UI_TEXT_BOX_COUNT] = {0};


static void ui__print(ui_text_box_e box, ui_color_pair_e color,
                      const char* format, ...);


static position_t ui__position_to_display(const position_t* pos)
{
	return (position_t) {
		UI_BORDER_WIDTH + 1 + (pos->x * UI_TILE_WIDTH),
		UI_BORDER_WIDTH + 1 + (pos->y * UI_TILE_HEIGHT)
	};
}


static bool ui__draw_square(const board_square_t* square,
                            const position_t*     position,
                            bool                  is_cursor,
                            bool                  on_board)
{
	if (!square || !position || (on_board && !board_valid_position(*position)))
		return false;

	ui_color_pair_e color                  = UI_COLOR_PAIR_NORM;
	char            text[UI_TILE_TEXT_MAX] = "";
	char*           temp_text              = "";

	switch (square->type)
	{
		case '-':
			color     = UI_COLOR_PAIR_NORM;
			temp_text = "  ";
			break;

		case '2':
			color     = UI_COLOR_PAIR_2;
			temp_text = "2L";
			break;

		case '3':
			color     = UI_COLOR_PAIR_3;
			temp_text = "3L";
			break;

		case 'd':
			color     = UI_COLOR_PAIR_D;
			temp_text = "DW";
			break;

		case 't':
			color     = UI_COLOR_PAIR_T;
			temp_text = "TW";
			break;

		case '*':
			color     = UI_COLOR_PAIR_D;
			temp_text = "{}";
			break;

		default:
		{
			ui_print_error("Warning: invalid square type %c", square->type);
			break;
		}
	}

	if (square->tile.face != 0)
	{
		text[0] = square->tile.face;
		text[1] = ' ';
		text[2] = '\0';
		color   = UI_COLOR_PAIR_TILE;
	}
	else
		strncpy(text, temp_text, UI_TILE_TEXT_MAX);

	if (is_cursor)
	{
		attron(A_BLINK);
		attron(A_BOLD);
		attron(A_UNDERLINE);
	}
	attron(COLOR_PAIR(color));

	position_t display_pos = ui__position_to_display(position);
	mvprintw(display_pos.y, display_pos.x, "%s", text);

	attroff(COLOR_PAIR(color));
	if (is_cursor)
	{
		attroff(A_BLINK);
		attroff(A_BOLD);
		attroff(A_UNDERLINE);
	}

	return true;
}


static void ui__init_colors(void)
{
	start_color();

	init_pair(UI_COLOR_PAIR_NONE, COLOR_WHITE, COLOR_BLACK);
	init_pair(UI_COLOR_PAIR_NORM, COLOR_BLACK, COLOR_WHITE);
	init_pair(UI_COLOR_PAIR_2,    COLOR_BLACK, COLOR_CYAN);
	init_pair(UI_COLOR_PAIR_3,    COLOR_WHITE, COLOR_BLUE);
	init_pair(UI_COLOR_PAIR_D,    COLOR_BLACK, COLOR_MAGENTA);
	init_pair(UI_COLOR_PAIR_T,    COLOR_WHITE, COLOR_RED);

	init_pair(UI_COLOR_PAIR_TILE,    COLOR_BLUE,  COLOR_YELLOW);
	init_pair(UI_COLOR_PAIR_TEXTBOX, COLOR_WHITE, COLOR_GREEN);
}


static bool ui__init_board(unsigned board_width)
{
	bool success = true;
	position_t      position;
	board_square_t* square;
	for (position.y = 0; position.y < board_width; position.y++)
	{
		for (position.x = 0; position.x < board_width; position.x++)
		{
			square = board_get_square(position);
			if (square)
				success &= ui__draw_square(square, &position, false, true);
			else
				success = false;
		}
	}

	return success;
}


static void ui__init_text(void)
{
	// TODO: properly implement text boxes (multi-line)
	/* Command box */
	ui_text_box[UI_TEXT_BOX_CMD].start = (position_t) { 1, n_row_term - 1 };
	ui_text_box[UI_TEXT_BOX_CMD].end   =
		(position_t) { n_col_term - 1, n_row_term - 1 };
	ui__print(UI_TEXT_BOX_CMD, UI_COLOR_PAIR_NONE, "");

	/* Error box */
	ui_text_box[UI_TEXT_BOX_ERROR].start = (position_t) { 1, 1 };
	ui_text_box[UI_TEXT_BOX_ERROR].end   =
		(position_t) { n_col_term - 1, 1 };
	ui__print(UI_TEXT_BOX_ERROR, UI_COLOR_PAIR_NONE, "");

	/* Score box */
	unsigned score_col = (15 * UI_TILE_WIDTH) + UI_BORDER_WIDTH - sizeof("Score:---");
	ui_text_box[UI_TEXT_BOX_SCORE].start = (position_t) { score_col, 2 };
	ui_text_box[UI_TEXT_BOX_SCORE].end   =
		(position_t) { score_col + sizeof("Score:---"), 2 };
	ui__print(UI_TEXT_BOX_SCORE, UI_COLOR_PAIR_NORM, "Score:---");
}


bool ui_init(tile_rack_t* rack)
{
	if (!rack) return false;

	tile_rack = rack;

	initscr();
	cbreak();
	ui__init_colors();

	keypad(stdscr, TRUE);
	noecho();

	curs_set(0);

	getmaxyx(stdscr, n_row_term, n_col_term);
	ui__init_text();

	board_width = board_get_width();
	unsigned ui_width    = (board_width * UI_TILE_WIDTH) + (2 * UI_BORDER_WIDTH);
	unsigned ui_height   = (board_width * UI_TILE_HEIGHT) + (2 * UI_BORDER_WIDTH);

	if (((unsigned)n_row_term < ui_height) || ((unsigned)n_col_term < ui_width))
	{
		ui__print(UI_TEXT_BOX_ERROR, UI_COLOR_PAIR_ERROR,
		          "Warning: terminal not big enough: (%d, %d) < (%u, %u)!!",
		          n_row_term, n_col_term, ui_height, ui_width);
	}

	bool success = ui__init_board(board_width);

	cursor.x = (board_width / 2);
	cursor.y = (board_width / 2);

	success &= ui_draw();

	return success;
}


bool ui__draw_cursor(void)
{
	bool success = true;

	board_square_t* cur_square = board_get_square(cursor);
	success &= ui__draw_square(cur_square, &cursor, true, true);

	if (wipe_cursor_last)
	{
		board_square_t* last_square = board_get_square(cursor_last);
		success &= ui__draw_square(last_square, &cursor_last, false, true);
	}

	return success;
}


bool ui__draw_rack(void)
{
	if (!tile_rack) return false;

	bool success = true;
	board_square_t square   = { .tile = {0}, .type = '-' };
	position_t     position =
		{ .x = ((board_width - tile_rack->size) / 2), .y = board_width + 1 };

	for (unsigned i = 0; i < tile_rack->size; i++)
	{
		square.tile = tile_rack->tile[i];
		success &= ui__draw_square(&square, &position, false, false);
		position.x++;
	}

	return success;
}


/* Get changes to board and ui and update display */
bool ui_draw(void)
{
	bool success = true;

	/* Detect window resize */
	int n_row, n_col;
	getmaxyx(stdscr, n_row, n_col);

	if ((n_row != n_row_term) || (n_col != n_col_term))
	{
		success &= ui__init_board(board_width);
		n_row_term = n_row;
		n_col_term = n_col;
	}

	/* Redraw ui text */
	for (unsigned i = 0; i < UI_TEXT_BOX_COUNT; i++)
	{
		attron(A_BOLD);
		attron(COLOR_PAIR(ui_text_box[i].color));
		unsigned box_width = ui_text_box[i].end.x - ui_text_box[i].start.x;
		mvprintw(ui_text_box[i].start.y, ui_text_box[i].start.x,
		         "%-*s", box_width, ui_text_box[i].text);
		attroff(A_BOLD);
		attroff(COLOR_PAIR(ui_text_box[i].color));
	}

	/* Get board changes and draw */
	board_square_t current_square;
	position_t     current_pos;
	unsigned num_placements = board_turn_length();
	for (unsigned i = 0;
	     (i < num_placements) && board_turn_get_index(
	        i, &current_square, &current_pos); i++)
		success &= ui__draw_square(&current_square, &current_pos, false, true);

	while (board_turn_deletes_pop(&current_square, &current_pos))
		success &= ui__draw_square(&current_square, &current_pos, false, true);

	success &= ui__draw_cursor();
	success &= ui__draw_rack();

	refresh();

	/* Reset Error box */
	ui__print(UI_TEXT_BOX_ERROR, UI_COLOR_PAIR_NONE, " ");
	return success;
}


static void ui__print(ui_text_box_e box, ui_color_pair_e color,
                      const char* format, ...)
{
	if (!format) return;

	if (format[0] == '\0')
		strncpy(ui_text_box[box].text, "", UI_TEXT_MAX);
	else
	{
		va_list args;
		va_start(args, format);

		vsnprintf(ui_text_box[box].text, UI_TEXT_MAX,
		          format, args);
		ui_text_box[box].color = color;

		va_end(args);
	}
}


void ui_print_error(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	vsnprintf(ui_text_box[UI_TEXT_BOX_ERROR].text, UI_TEXT_MAX,
	          format, args);
	ui_text_box[UI_TEXT_BOX_ERROR].color = UI_COLOR_PAIR_ERROR;

	va_end(args);
}


bool ui_move(ui_command_mv_e direction)
{
	if (direction >= UI_COMMAND_MV_COUNT) return false;

	unsigned dx = 0, dy = 0;
	switch (direction)
	{
		case UI_COMMAND_MV_UP:
			dy = -1;
			break;

		case UI_COMMAND_MV_DOWN:
			dy = 1;
			break;

		case UI_COMMAND_MV_LEFT:
			dx = -1;
			break;

		case UI_COMMAND_MV_RIGHT:
			dx = 1;
			break;

		default:
			break;
	}

	position_t new_cursor = (position_t) { cursor.x + dx, cursor.y + dy };

	if (!board_valid_position(new_cursor))
		return false;

	cursor_last      = cursor;
	wipe_cursor_last = true;

	cursor = new_cursor;
	return true;
}


void ui_update_score(unsigned score)
{
	ui__print(UI_TEXT_BOX_SCORE, UI_COLOR_PAIR_NORM, "Score: %-3u", score);
}


void ui_set_mode(ui_mode_e mode)
{
	if (mode >= UI_MODE_COUNT) return;

	if (mode == UI_MODE_ON_BOARD)
		ui__print(UI_TEXT_BOX_CMD, UI_COLOR_PAIR_NONE, " ");

	ui_mode = mode;
}


ui_command_t ui__process_input_on_board(int ch)
{
	ui_command_t cmd = { .type = UI_COMMAND_NONE };
	switch (ch)
	{
		case ':':
			ui__print(UI_TEXT_BOX_CMD, UI_COLOR_PAIR_TEXTBOX, ":");
			cmd.type      = UI_COMMAND_MODE;
			cmd.data.mode = UI_MODE_COLON;
			break;

		case KEY_UP:
			cmd.data.direction = UI_COMMAND_MV_UP;
			cmd.type = UI_COMMAND_MV;
			break;

		case KEY_DOWN:
			cmd.data.direction = UI_COMMAND_MV_DOWN;
			cmd.type = UI_COMMAND_MV;
			break;

		case KEY_RIGHT:
			cmd.data.direction = UI_COMMAND_MV_RIGHT;
			cmd.type = UI_COMMAND_MV;
			break;

		case KEY_LEFT:
			cmd.data.direction = UI_COMMAND_MV_LEFT;
			cmd.type = UI_COMMAND_MV;
			break;

		case KEY_ESC_:
			cmd.type = UI_COMMAND_ESC;
			break;

		case KEY_ENTER_:
			cmd.type = UI_COMMAND_ENTER;
			break;

		case KEY_DC:
			cmd.type         = UI_COMMAND_DEL;
			cmd.data.cmd_pos = cursor;
			break;

		default:
		{
			/* Send tile CMD if valid */
			tile_t tile;
			if (((tile = as_tile(ch)).face) != 0)
			{
				cmd.type          = UI_COMMAND_ADD_TILE;
				cmd.data.cmd_tile = tile.face;
				cmd.data.cmd_pos  = cursor;
			}
			break;
		}
	}

	return cmd;
}


ui_command_t ui__process_input_colon(int ch)
{
	ui_command_t cmd = { .type = UI_COMMAND_NONE };
	static int last_char = 0;
	switch (ch)
	{
		case KEY_ESC_:
			last_char     = 0;
			cmd.type      = UI_COMMAND_MODE;
			cmd.data.mode = UI_MODE_ON_BOARD;
			break;

		case KEY_ENTER_:
			// TODO: suppport multiple characters
			if (last_char)
			{
				switch (last_char)
				{
					case 'q':

						cmd.type = UI_COMMAND_QUIT;
						break;

					case 's':

						cmd.type = UI_COMMAND_RERACK;
						break;

					default:
						break;
				}
				last_char = 0;
			}
			break;

		case KEY_BACKSPACE:
			last_char = 0;
			string_backspace(ui_text_box[UI_TEXT_BOX_CMD].text, UI_TEXT_MAX);
			break;

		default:
			string_cat_char(ui_text_box[UI_TEXT_BOX_CMD].text, ch,
			                UI_TEXT_MAX);
			last_char = ch;
			break;
	}

	return cmd;
}


ui_command_t ui_get_cmd(void)
{
	int ch = getch();

	ui_command_t cmd = { .type = UI_COMMAND_NONE };

	switch (ui_mode)
	{
		case (UI_MODE_ON_BOARD):
			cmd = ui__process_input_on_board(ch);
			break;

		case (UI_MODE_COLON):
			cmd = ui__process_input_colon(ch);
			break;

		default:
			break;
	}

	return cmd;
}


void ui_tear_down(void)
{
	clrtoeol();
	endwin();
}
