#ifndef __BOARD_H__
#define __BOARD_H__
/* The `board_t` contains all of the `board_square_t`s and the `tile_t`s placed
 * on them board.
 */

#include <stdbool.h>
#include <stddef.h>

#include "tiles.h"
#include "utils.h"

#define BOARD_MAX_WIDTH 15
#define WORD_MAX_LENGTH 32

/* Square type */
typedef enum
{
	BOARD_SQUARE_TYPE_NORMAL     = '-',
	BOARD_SQUARE_TYPE_DUB_LETTER = '2',
	BOARD_SQUARE_TYPE_TRI_LETTER = '3',
	BOARD_SQUARE_TYPE_DUB_WORD   = 'd',
	BOARD_SQUARE_TYPE_TRI_WORD   = 't',
	BOARD_SQUARE_TYPE_CENTER     = '*',
} board_square_type_e;

/* The `board_t` contains a set of squares, each with a type (see above) and
 * an (optional) tile. */
typedef struct
{
	board_square_type_e type;
	tile_t              tile;
} board_square_t;

/* Word string and square type lumped together - easier to pass around
 * like this */
typedef struct
{
	board_square_t      square[WORD_MAX_LENGTH];
	size_t              length;
} board_word_t;

static const board_word_t word_empty = {0};

/* This is written so as to support different sized boards in the future, the
 * current code only supports the standard scrabble board (15x15) */
typedef struct
{
	board_square_t* square;
	const unsigned  width;
} board_t;

/* Initialises the state of the `board_t` that is statically allocated in
 * board.c */
void     board_init     (unsigned width);
unsigned board_get_width(void);

/* Adds a `tile_t` to the board at a `position_t`. This function can fail,
 * and so returns a success bool */
bool board_add_tile(tile_t     tile,
                    position_t position);


/* Tests if the passed `position_t` is within the limits of the board */
bool board_valid_position(position_t position);

/* Gets a pointer (the address in memory) to the square at position,
 * can return NULL of the position is invalid */
board_square_t* board_get_square(position_t position);

/* The following functions deal with the turn on the board */
/* Finalise turn and reset the diff stack */
void board_turn_complete(void);
/* Get the number of placements in the turn */
unsigned board_turn_length(void);
/* Get the position and/or square at a given index in the turn, note that
 * the user code has to provide a square and a position to write to */
bool board_turn_get_index(
	int index, board_square_t* square, position_t* position);
/* Get the square (with tile) at a given position (if that position is
 * contained within the turn) */
bool board_turn_pop_position(position_t position, board_square_t* square);
/* Pop the last change off the diff stack */
bool board_turn_pop(board_square_t* square, position_t* position);
/* Pop the last delete operation off the delete stack */
bool board_turn_deletes_pop(board_square_t* square, position_t* position);

/* Get a set of words that were made during the turn
 *
 * words     - an array of `board_word_t`s of size `*words_len`
 * words_len - the user passes this with the max capacity of `words` and the
 *             function writes how many words were returned
 * returns   - success bool
 */
bool board_turn_get_words(board_word_t* words, size_t* words_len);

#endif
