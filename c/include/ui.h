#ifndef __UI_H__
#define __UI_H__
/* The ui contains all the code that delas with user input and drawing the
 * program state to the screen */
#include <stdarg.h>
#include <stdbool.h>

#include "board.h"
#include "tiles.h"

/* Commands are passed from the ui to the main loop */
typedef enum
{
	UI_COMMAND_NONE,
	UI_COMMAND_MV,
	UI_COMMAND_ADD_TILE,
	UI_COMMAND_ENTER,
	UI_COMMAND_MODE,
	UI_COMMAND_DEL,
	UI_COMMAND_ESC,

	UI_COMMAND_RERACK,
	UI_COMMAND_QUIT,

	UI_COMMAND_COUNT
} ui_command_e;

typedef enum
{
	UI_COMMAND_MV_UP,
	UI_COMMAND_MV_DOWN,
	UI_COMMAND_MV_LEFT,
	UI_COMMAND_MV_RIGHT,

	UI_COMMAND_MV_COUNT
} ui_command_mv_e;

typedef enum
{
	UI_MODE_ON_BOARD,
	UI_MODE_COLON,

	UI_MODE_COUNT
} ui_mode_e;

/* This struct is a tagged union, containing the type of command and
 * associated data */
typedef struct
{
	ui_command_e type;
	union
	{
		struct
		{
			position_t cmd_pos;
			char       cmd_tile;
		};
		ui_command_mv_e direction;
		ui_mode_e       mode;
	} data;
} ui_command_t;

/* Initialises the ui, takes a pointer to the main loop's `tile_rack_t`, so
 * it can update the rack on every draw */
bool ui_init(tile_rack_t* rack);
/* Opposite of init - calls all the necessary resource deallocation functions */
void ui_tear_down(void);

/* Called once every pass through the main loop - draws the program state */
bool ui_draw(void);

/* Useful function for writing directly to the ui error textbox, follows the
 * same basic function signature as printf */
void ui_print_error(const char* format, ...) __attribute__((format(printf, 1, 2)));
/* Move the cursor in a given direction */
bool ui_move(ui_command_mv_e direction);
/* Update the score displayed */
void ui_update_score(unsigned score);
/* Set ui mode (currently supports ON_BOARD and COMMAND modes) */
void ui_set_mode(ui_mode_e mode);

/* Get the command from the user. The program will hang here until the user
 * provides an input */
ui_command_t ui_get_cmd(void);

#endif
