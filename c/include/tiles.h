#ifndef __TILES_H__
#define __TILES_H__
/* Contains the tile management functions and structs */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define TILES_NUM_MAX 100
/* The face is the character the tile represents, and the `is_blank` bool
 * allows for the wildcards to be assigned a face value by the user (for
 * word validation), whilst allowing for the recovery of the *actual* face
 * value if the user decides to delete the tile from the board */
typedef struct
{
	char face;
	bool is_blank;
} tile_t;

/* A tile bag is a set of shuffled `tile_t`s of length TILES_NUM_MAX */
typedef struct tile_bag_s tile_bag_t;

tile_bag_t* tile_bag_create (void);
void        tile_bag_delete (tile_bag_t* bag);
void        tile_bag_shuffle(tile_bag_t* bag);
tile_t      tile_bag_pick   (tile_bag_t* bag);
bool        tile_bag_empty  (const tile_bag_t* bag);
/* Add a set of `tiles` (array of length `tile_count`) back into the bag */
size_t      tile_bag_add    (
	tile_bag_t* bag, const tile_t* tiles, size_t tile_count);

/* Get the score value of the tile */
unsigned tile_value(tile_t tile);
/* Make a `tile_t` from a `char` */
tile_t as_tile(char ch);

static inline bool tile_occupied(tile_t *tile)
{
	return (tile->face != 0);
}


/* A tile rack is the set of tiles the player has access to in a given turn */
#define TILE_RACK_SIZE 7
typedef struct
{
	tile_t tile[TILE_RACK_SIZE];
	size_t size;
} tile_rack_t;

tile_rack_t tile_rack_create(void);
/* Top the rack up with tiles from the bag */
void        tile_rack_topup (tile_rack_t* rack, tile_bag_t* bag);
/* Remove the tile with face == `tile_face` from the rack. If it doesn't exist,
 * then the function returns false. The `tile_t` (which may be a wildcard `_`
 * tile is written to `out`
 */
bool        tile_rack_remove(tile_rack_t* rack, char tile_face, tile_t* out);
/* Puts the `tile` baack on the rack. If there's no space, returns false. */
bool        tile_rack_add   (tile_rack_t* rack, tile_t tile);
/* Clears everything off the rack, resetting it. */
void        tile_rack_clear (tile_rack_t* rack);

#endif
