#ifndef __UTILS_H__
#define __UTILS_H__

#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#define ABS_SUB(a, b) (a < b) ? b - a : a - b

/* Converts the char to an uppercase letter */
static inline char letter_upper(char letter)
{
	return isalpha(letter) ? toupper(letter) : 0;
}


/* Position object used by the board and ui */
typedef struct
{
	unsigned x, y;
} position_t;

static inline bool position_equal(position_t a, position_t b)
{
	return (a.x == b.x) && (a.y == b.y);
};

static inline bool position_lt(position_t a, position_t b)
{
	return (a.x < b.x) || (a.y < b.y);
};

static inline bool position_gt(position_t a, position_t b)
{
	return (a.x > b.x) || (a.y > b.y);
};

static inline position_t position_add(position_t a, position_t b)
{
	return (position_t) { (a.x + b.x), (a.y + b.y) };
};

static inline unsigned position_manhattan_dist(position_t a, position_t b)
{
	return (ABS_SUB(b.x, a.x) + ABS_SUB(b.y, a.y));
};


/* Add nul 1 character earlier to the string (backspace) */
static inline void string_backspace(char* str, size_t max_len)
{
	if (!str) return;

	unsigned len = strnlen(str, max_len);

	if ((len < max_len) && (len > 0))
		str[len - 1] = '\0';
}

/* Add character to nul terminated string */
static inline void string_cat_char(char* str, char ch, size_t max_len)
{
	if (!str) return;

	unsigned len = strnlen(str, max_len);

	if (len < max_len - 1)
	{
		str[len]     = ch;
		str[len + 1] = '\0';
	}
}

/* Get the last char in the string */
static inline char string_get_last_char(const char* str, size_t max_len)
{
	if (!str) return 0;

	unsigned len = strnlen(str, max_len);

	if (len > 0)
		return str[len - 1];
	else
		return '\0';
}

#endif
