#ifndef __WORD_H__
#define __WORD_H__

#include <stdint.h>
#include <stdbool.h>

#include "board.h"

/* Get the number of consecutive matches against a char string */
size_t word_string_match(const board_word_t* word, const char* str);

/* Convert char string (with optional type string) to `board_word_t`
 *
 * str  - nul terminated (has the '\0' char somewhere to indicate the string
 *        should not be parsed further) string to be converted
 * type - nul terminated `board_square_type_e` string
 *
 * returns `board_word_t` object of length
 */
board_word_t as_word(const char* str, char* type);

/* Score word according to scrabble rules (look at `tile__value` in tiles.c)
 *
 * word - struct which bundles together the tile and board squares (with the
 *        multipliers) - note that you can use the `tile_value` function (from
 *        tile.h) to get the value of the tile
 */
unsigned word_score(board_word_t word);

/* Check if the passed word is valid according to the word_list_file
 *
 * word - word to check against (you don't need to worry about the squares in
 *        the word here)
 * word_list_file - filename of file that contains the list of valid words
 *                  (see WORD_LIST_FILE in main.c)
 *
 * returns bool whether or not the word is valid
 */
bool is_word(board_word_t word, const char* word_list_file);

#endif
